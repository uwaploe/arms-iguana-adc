# Versalogic Iguana ADC driver

This repository contains a Linux driver module for the internal A/D
converter on a
[Versalogic Iguana](http://versalogic.com/products/ds.asp?productid=229)
single-board computer. The module provides a simple *Procfs* based
interface and is only suitable for low-speed sampling (< 100 hz). The
driver also provides a interface to a 16 line DIO chip which shares the
same SPI bus with the A/D.

## Loading

``` shellsession
modprobe iguana_adc [[output_mask=<N>] enable_pullups=<1|0>]
```

The parameter *output_mask* is used to specify the DIO lines which should
be configured as outputs. The *enable_pullups* parameter enables a 100kΩ
pull-up resistor on all lines configured as inputs.

## Usage

Read `/proc/iguana/adc` to obtain the latest ASCII data record. Each
record starts with a timestamp in seconds and microseconds since 1/1/1970
UTC and consists of eight decimal values terminated with a line-feed. The
units are A/D counts.

Read `/proc/iguana/dio` to read the state of the DIO lines an ASCII
encoded hex integer.

Write two 16-bit words to `/proc/iguana/dio` to change the state of the
output lines. The words must be sent least-significant byte first. The
first word is the value and the second word is a mask which selects the
bits to modify. As an example, consider a case where the first 8 lines are
output and we want to set lines 0 and 2 high and set the rest low:

``` shellsession
$ echo -n -e '\x05\x00\xff\x00' > /proc/iguana/dio
```
