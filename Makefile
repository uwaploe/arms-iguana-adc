#
# Makefile for Iguana A/D driver
#

obj-m += iguana_adc.o

KERNEL_DIR ?= /lib/modules/$(shell uname -r)/build

all: modules

modules:
	$(MAKE) -C $(KERNEL_DIR) M=$$PWD modules

clean:
	rm -rf *.o *~ core .depend .*.cmd *.ko *.mod.c .tmp_versions modules.order Module.symvers dio *.tmp *.log
