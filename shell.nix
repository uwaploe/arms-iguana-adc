with import <nixpkgs> {}; {
     kEnv = stdenv.mkDerivation {
       name = "kmodbuild";
       buildInputs = [ stdenv linux ];
       KERNEL_DIR="${linux.dev}/lib/modules/${linux.modDirVersion}/build";
     };
}