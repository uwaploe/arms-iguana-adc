/*
 * iguana_adc
 *
 * Simple procfs-based interface to read the internal A/D on a Versalogic
 * Iguana SBC. The ADC is sampled by reading /proc/iguana/adc. The data
 * record is a single line of space separated ASCII values:
 *
 *   TIMESTAMP Ain0 Ain1 Ain2 ... Ain7
 *
 * TIMESTAMP is a floating point value; SECONDS.MICROSECONDS since the epoch
 *
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/mutex.h>
#include <asm/signal.h>
#include <linux/timekeeping.h>
#include <linux/time.h>
#include <linux/slab.h>
#include <asm/io.h>
#include <linux/delay.h>
#include <linux/time.h>
#include <linux/printk.h>
#include <linux/proc_fs.h>
#include <linux/jiffies.h>
#include <linux/wait.h>
#include <linux/seq_file.h>
#include <linux/uaccess.h>

#define DRVDESC "Versalogic Iguana A/D driver"

/* Versalogic Iguana SPIX register definitions */
#define SPIX_BASE       0xca8
#define SPIX_NREGS      6
#define SPI_CONTROL     0
#define SPI_STATUS      1
#define SPI_DATA0       2
#define SPI_DATA1       3
#define SPI_DATA2       4
#define SPI_DATA3       5

/* bitmasks */
#define SPI_CPOL_HIGH   0x80
#define SPI_CPHA_FALL   0x40
#define SPI_LEN_8       0x00
#define SPI_LEN_16      (0x01 << 4)
#define SPI_LEN_24      (0x02 << 4)
#define SPI_LEN_32      (0x03 << 4)
#define SPI_MAN_SS      0x08
#define SPI_IRQ_3       0x00
#define SPI_IRQ_4       (0x01 << 6)
#define SPI_IRQ_5       (0x02 << 6)
#define SPI_IRQ_10      (0x03 << 6)
#define SPI_CLK_1MHZ    0x00
#define SPI_CLK_2MHZ    (0x01 << 4)
#define SPI_CLK_4MHZ    (0x02 << 4)
#define SPI_CLK_8MHZ    (0x03 << 4)

#define SPI_IRQ_ENABLE      0x08
#define SPI_IRQ_ASSERT      0x02
#define SPI_BUSY            0x01
#define SPI_WRITE_SLAVE     0x40
#define SPI_READ_SLAVE      0x41

#define SPI_DIO_ADDR        0x06
#define SPI_ADC_ADDR        0x05

/* SPI_CONTROL value to access digital i/o chip */
#define SPI_DIO_CONTROL (SPI_LEN_24 | SPI_DIO_ADDR)
/* SPI_STATUS value to access digital i/o chip */
#define SPI_DIO_STATUS SPI_CLK_8MHZ
/* SPI_CONTROL value to access the A/D chip */
#define SPI_ADC_CONTROL (SPI_LEN_16 | SPI_ADC_ADDR)
/* SPI_STATUS value to access the A/D chip */
#define SPI_ADC_STATUS SPI_CLK_2MHZ

/* MCP23X17 I/O chip registers, BANK=0 */
#define IODIRA      0x00
#define IODIRB      0x01
#define IPOLA       0x02
#define IPOLB       0x03
#define GPINTENA    0x04
#define GPINTENB    0x05
#define DEFVALA     0x06
#define DEFVALB     0x07
#define INTCONA     0x08
#define INTCONB     0x09
#define IOCONA      0x0a
#define IOCONB      0x0b /* same as IOCONA */
#define GPPUA       0x0c
#define GPPUB       0x0d
#define INTFA       0x0e
#define INTFB       0x0f
#define INTCAPA     0x10
#define INTCAPB     0x11
#define GPIOA       0x12
#define GPIOB       0x13
#define OLATA       0x14
#define OLATB       0x15

/* ADC macros */
#define ADC_CHANNELS    8
#define ADC_CTL_REG     0xcaf
#define ADC_NREGS       1

/* bitmasks */
#define ADC_CONV        0x01
#define ADC_BUSY        0x04
#define ADC_VRANGE_5PP  0x00
#define ADC_VRANGE_10PP (0x01 << 2)
#define ADC_VRANGE_5P   (0x02 << 2)
#define ADC_VRANGE_10P  (0x03 << 2)
#define ADC_SE          0x80

/* Module parameters */
static unsigned int output_mask = 0;
static unsigned int enable_pullups = 0;
MODULE_PARM_DESC(output_mask, "Mask of DIO lines configured as outputs");
module_param(output_mask, uint, 0);
MODULE_PARM_DESC(enable_pullups, "Inf non-zero enable input pull-up resistors");
module_param(enable_pullups, uint, 0);

/*
 * Convert A/D channel numbers to channel codes. This code is written
 * to bits 4-6 of the A/D command byte. See page 48 of the Iguana
 * manual for the gory details (if you dare :-)
 */
static unsigned int ad_chan_codes[ADC_CHANNELS] = {
    0x00, 0x04, 0x01, 0x05, 0x02, 0x06, 0x03, 0x07
};

struct iguana_adc_data {
    struct mutex            lock;
    void                    *spi_membase;
    void                    *adc_membase;
    struct proc_dir_entry   *dir;
    struct timeval          tv_sample;
    unsigned long           tstamp;
    unsigned int            samples[ADC_CHANNELS];
    int                     gpio_a;
    int                     gpio_b;
};

static struct iguana_adc_data *this_dev = NULL;


#ifdef DEBUG_MODE
static void
dump_spi(void *membase)
{
    int i, ctl, stat, data[4];

    ctl = ioread8(membase + SPI_CONTROL);
    stat = ioread8(membase + SPI_STATUS);
    for(i = 0;i < 4;i++)
        data[i] = ioread8(membase + SPI_DATA0 + i);
    pr_info("SPI regs: %02x/%02x %02x/%02x/%02x/%02x\n",
            ctl, stat, data[0], data[1], data[2], data[3]);
}
#endif

/*
 * Wait for SPI transfer to complete. Returns 0 on success
 * or -ETIMEDOUT on timeout.
 */
static int
spi_wait(void *membase)
{
    int timeout = 10;

    while(ioread8(membase + SPI_STATUS) & SPI_BUSY)
    {
        rmb();
        udelay(5);
        if(timeout-- < 0)
            return -ETIMEDOUT;
    }

    return 0;
}

/*
 * Wait for A/D conversion to complete. Returns 0 on success
 * or -ETIMEDOUT on timeout.
 */
static int
adc_wait(void *membase)
{
    int timeout = 10;

    while(ioread8(membase) & ADC_BUSY)
    {
        rmb();
        udelay(5);
        if(timeout-- < 0)
            return -ETIMEDOUT;
    }

    return 0;
}

/*
 * Write to a register on the MCP23X17 I/O chip
 */
static int
write_io_register(void *membase, unsigned int reg, unsigned int value)
{
    iowrite8(value, membase + SPI_DATA1);
    iowrite8(reg, membase + SPI_DATA2);
    wmb();
    iowrite8(SPI_WRITE_SLAVE, membase + SPI_DATA3);
    wmb();
    return spi_wait(membase);
}

/*
 * Read a register on the MCP23X17 I/O chip
 */
static int
read_io_register(void *membase, unsigned int reg)
{
    int rval;

    iowrite8(0, membase + SPI_DATA1);
    iowrite8(reg, membase + SPI_DATA2);
    wmb();
    iowrite8(SPI_READ_SLAVE, membase + SPI_DATA3);
    wmb();
    rval = spi_wait(membase);

    return rval == 0 ? ioread8(membase + SPI_DATA1) : rval;
}

static int
mod_io_register(void *membase, unsigned int reg, unsigned val, unsigned mask)
{
    unsigned int regval;

    regval = read_io_register(membase, reg);
    regval = (regval & ~mask) | (val & mask);
    return write_io_register(membase, reg, regval);
}

static int
dio_init(struct iguana_adc_data *dev)
{
    int             rval;
    unsigned int    dir = ~output_mask;

    iowrite8(SPI_DIO_CONTROL, dev->spi_membase + SPI_CONTROL);
    iowrite8(SPI_DIO_STATUS, dev->spi_membase + SPI_STATUS);

    /* Zero the GPIO registers */
    if((rval = write_io_register(dev->spi_membase, GPIOA, 0)) < 0)
        return rval;
    if((rval = write_io_register(dev->spi_membase, GPIOB, 0)) < 0)
        return rval;

    /*
     * A zero in the direction register configures the corresponding
     * line as an output.
     */
    if((rval = write_io_register(dev->spi_membase, IODIRA, dir & 0xff)) < 0)
        return rval;
    if((rval = write_io_register(dev->spi_membase, IODIRB, (dir >> 8) & 0xff)) < 0)
        return rval;
    if(enable_pullups)
    {
        write_io_register(dev->spi_membase, GPPUA, dir & 0xff);
        write_io_register(dev->spi_membase, GPPUB, (dir >> 8) & 0xff);
    }

    /* Drive the output latches */
    if((rval = write_io_register(dev->spi_membase, OLATA, 0)) < 0)
        return rval;
    if((rval = write_io_register(dev->spi_membase, OLATB, 0)) < 0)
        return rval;

    return 0;
}

static void
dio_sample(struct iguana_adc_data *dev)
{
    iowrite8(SPI_DIO_CONTROL, dev->spi_membase + SPI_CONTROL);
    iowrite8(SPI_DIO_STATUS, dev->spi_membase + SPI_STATUS);
    dev->gpio_a = read_io_register(dev->spi_membase, GPIOA);
    dev->gpio_b = read_io_register(dev->spi_membase, GPIOB);
}

static void
adc_sample(struct iguana_adc_data *data)
{
    int                     chan, i;
    unsigned int            ad_val, ad_cmd;
    unsigned long           tstamp;
    unsigned int            sample_buf[ADC_CHANNELS];

    do_gettimeofday(&data->tv_sample);

    /*
     * Configure SPI bus transactions.
     */
    iowrite8(SPI_ADC_CONTROL, data->spi_membase + SPI_CONTROL);
    iowrite8(SPI_ADC_STATUS, data->spi_membase + SPI_STATUS);
    tstamp = jiffies;

    /*
     * Each SPI transaction with the ADC returns the sample from the *previous*
     * conversion, therefore, we need to request ADC_CHANNELS+1 conversions
     * to cycle through all of the channels.
     */
    wmb();
    for(chan = 0,i = -1;chan <= ADC_CHANNELS;chan++,i++)
    {
        ad_cmd = (ad_chan_codes[chan%ADC_CHANNELS] << 4) | ADC_VRANGE_5P | ADC_SE;
        iowrite8(ad_cmd, data->spi_membase + SPI_DATA3);
        wmb();
        if(spi_wait(data->spi_membase) < 0)
        {
            pr_err("SPI timeout\n");
            break;
        }

        if(i >= 0)
        {
            /* read the result of the previous conversion */
            ad_val = ioread8(data->spi_membase + SPI_DATA3);
            sample_buf[i] = (ad_val << 4) |
              (ioread8(data->spi_membase + SPI_DATA2) >> 4);
        }
        /* Delay at least 1.5usecs between SPI cycle and A/D conversion */
        udelay(2);
        /* Start the conversion */
        iowrite8(ADC_CONV, data->adc_membase);
        wmb();
        /* Wait until the converter is ready */
        if(adc_wait(data->adc_membase) < 0)
        {
            pr_err("ADC timeout\n");
            break;
        }
    }
    /* Copy data to user-accessible buffer */
    memcpy(&data->samples[0], sample_buf, sizeof(sample_buf));
    data->tstamp = tstamp;
}

/*
 * /proc interface to read the most recent A/D sample.
 */
static int
adc_data_show(struct seq_file *m, void *arg)
{
    int                     i;
    struct timeval          tv;
    unsigned int            sample_buf[ADC_CHANNELS];
    struct iguana_adc_data *data = (struct iguana_adc_data*)m->private;

    if(mutex_lock_interruptible(&data->lock))
        return -ERESTARTSYS;
    adc_sample(data);
    memcpy(sample_buf, &data->samples[0], sizeof(sample_buf));
    tv.tv_sec = data->tv_sample.tv_sec;
    tv.tv_usec = data->tv_sample.tv_usec;
    mutex_unlock(&data->lock);

    seq_printf(m, "%ld.%06ld", tv.tv_sec, tv.tv_usec);
    for(i = 0;i < ADC_CHANNELS;i++)
        seq_printf(m, " %d", sample_buf[i]);
    seq_printf(m, "\n");

    return 0;
}

/*
 * /proc interface to read DIO ports.
 */
static int
dio_data_show(struct seq_file *m, void *arg)
{
    int                     val;
    struct iguana_adc_data *data = (struct iguana_adc_data*)m->private;

    if(mutex_lock_interruptible(&data->lock))
        return -ERESTARTSYS;
    dio_sample(data);
    val = (data->gpio_b << 8) | data->gpio_a;
    mutex_unlock(&data->lock);

    seq_printf(m, "0x%04x\n", val);
    return 0;
}

static ssize_t
dio_write(struct file* file, const char __user* buf, size_t size, loff_t* pos)
{
    unsigned char           kbuf[4];
    ssize_t                 rval = size;
    struct seq_file         *m = (struct seq_file*)file->private_data;
    struct iguana_adc_data  *dev = (struct iguana_adc_data*)m->private;

    if(size < 4)
        return -EINVAL;

    if(mutex_lock_interruptible(&dev->lock))
        return -ERESTARTSYS;

    if(copy_from_user(kbuf, buf, 4))
    {
        rval = -EFAULT;
        goto done;
    }

    iowrite8(SPI_DIO_CONTROL, dev->spi_membase + SPI_CONTROL);
    iowrite8(SPI_DIO_STATUS, dev->spi_membase + SPI_STATUS);
    /*
     * Each write must be at least four bytes.
     *
     * The first two bytes are the register value (low, high) and
     * the second two bytes are the mask (low, high). If the mask
     * is zero, no bits are modified.
     */
    if(kbuf[2] > 0)
    {
        rval = mod_io_register(dev->spi_membase, GPIOA, kbuf[0], kbuf[2]);
        rval = mod_io_register(dev->spi_membase, OLATA, kbuf[0], kbuf[2]);
    }
    if(kbuf[3] > 0)
    {
        rval = mod_io_register(dev->spi_membase, GPIOB, kbuf[1], kbuf[3]);
        rval = mod_io_register(dev->spi_membase, OLATB, kbuf[1], kbuf[3]);
    }
done:
    mutex_unlock(&dev->lock);

    return rval < 0 ? rval : size;
}

static int
iguana_adc_proc_open(struct inode *inode, struct file *file)
{
    return single_open(file, adc_data_show, PDE_DATA(file_inode(file)));
}

static const struct file_operations iguana_adc_proc_ops = {
    .open       = iguana_adc_proc_open,
    .read       = seq_read,
    .llseek     = seq_lseek,
    .release    = single_release,
};

static int
iguana_dio_proc_open(struct inode *inode, struct file *file)
{
    return single_open(file, dio_data_show, PDE_DATA(file_inode(file)));
}

static const struct file_operations iguana_dio_proc_ops = {
    .open       = iguana_dio_proc_open,
    .read       = seq_read,
    .write      = dio_write,
    .llseek     = seq_lseek,
    .release    = single_release,
};

int __init
iguana_adc_init(void)
{
    int     rval;

    pr_info(DRVDESC "\n");
    this_dev = (struct iguana_adc_data*)kzalloc(sizeof(struct iguana_adc_data),
                                                  GFP_KERNEL);
    if(!this_dev)
    {
        pr_err("memory allocation failed\n");
        return -ENOMEM;
    }

    if(!request_region(SPIX_BASE, SPIX_NREGS, "iguana_adc"))
    {
        kfree(this_dev);
        pr_err("cannot access SPI register address\n");
        return -ENODEV;
    }

    if(!request_region(ADC_CTL_REG, ADC_NREGS, "iguana_adc"))
    {
        release_region(SPIX_BASE, SPIX_NREGS);
        kfree(this_dev);
        pr_err("cannot access SPI register address\n");
        return -ENODEV;
    }


    this_dev->spi_membase = ioport_map(SPIX_BASE, SPIX_NREGS);
    this_dev->adc_membase = ioport_map(ADC_CTL_REG, ADC_NREGS);

    if((rval = dio_init(this_dev)) < 0)
    {
        ioport_unmap(this_dev->spi_membase);
        ioport_unmap(this_dev->adc_membase);
        release_region(ADC_CTL_REG, ADC_NREGS);
        release_region(SPIX_BASE, SPIX_NREGS);
        kfree(this_dev);
        pr_err("cannot initialize DIO interface\n");
        return rval;
    }

    this_dev->dir = proc_mkdir("iguana", NULL);
    if(!this_dev->dir)
    {
        ioport_unmap(this_dev->spi_membase);
        ioport_unmap(this_dev->adc_membase);
        release_region(ADC_CTL_REG, ADC_NREGS);
        release_region(SPIX_BASE, SPIX_NREGS);
        kfree(this_dev);
        pr_err("cannot create /proc/iguana directory\n");
        return -ENOMEM;
    }

    mutex_init(&this_dev->lock);

    proc_create_data("adc", 0, this_dev->dir, &iguana_adc_proc_ops,
                     (void*)this_dev);
    proc_create_data("dio", 0666, this_dev->dir, &iguana_dio_proc_ops,
                     (void*)this_dev);

    return 0;
}

void __exit
iguana_adc_exit(void)
{
    remove_proc_entry("adc", this_dev->dir);
    remove_proc_entry("dio", this_dev->dir);
    mutex_destroy(&this_dev->lock);
    remove_proc_entry("iguana", NULL);
    ioport_unmap(this_dev->spi_membase);
    ioport_unmap(this_dev->adc_membase);
    release_region(ADC_CTL_REG, ADC_NREGS);
    release_region(SPIX_BASE, SPIX_NREGS);
    kfree(this_dev);
}

module_init(iguana_adc_init);
module_exit(iguana_adc_exit);
MODULE_AUTHOR("Michael Kenney <mikek@apl.uw.edu>");
MODULE_LICENSE("GPL");
